import React, {useState, useEffect} from 'react'; 



const TransactionHead = ({transaction}) => {

 return (	
 	<React.Fragment> 
 		 <h4 className="badge badge-success">{transaction.transactionCode}</h4>
		 <h4 className="badge badge-primary">{transaction.dateCreated}</h4> 
		 <h4 className="badge badge-warning">{transaction.status}</h4> 

 	</React.Fragment> 

 	)

}

export default TransactionHead;
