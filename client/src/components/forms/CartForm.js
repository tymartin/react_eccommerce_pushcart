import React from 'react'; 
 

const CartForm = ({cart, handleClearCart, handleRemoveItem, total}) => {

const handleCheckout=() => {
	let orders = cart.map(item => {
		return {
			id: item._id,
			qty: 1
		}
	})

	fetch("http://localhost:3001/transactions", {
		method: "POST",
		headers : {
			"content-Type" : "application/json", 
			"Authorization" : localStorage.getItem('token')
		}, 
		body : JSON.stringify({orders})
	})
	.then(data => data.json())
	.then(result => console.log(result))
}
return (

		<div className="container">
			<div className="row">
				<div className="col-12 py-3">
				{
					cart.length ?
					<React.Fragment>
					<h1 className="bg-dark text-white col-10 ml-5">My Cart</h1> 	
					 	<table class="table table-striped col-10 ml-5">
						  <thead>
						    <tr>
						      <th scope="col">Item Name</th>
						      <th scope="col">Price</th>
						      <th scope="col">Action</th>
						    </tr>
						  </thead>
						  <tbody>
						  {
						  	cart.map( item => {
						  		return (
						  		<tr>
								      <td>{item.name}</td>
								      <td>{item.price}</td>
								      <td><button onClick={()=>{handleRemoveItem(item)}} className="btn btn-danger">Remove from Cart</button></td>
						   		 </tr>
						  		)
						  	})
						  }
						  </tbody>
						  <tfoot> 
						  	<td className="text-right"> Total </td> 
						  	<td> {total}</td> 
						  	<td><button onClick={handleCheckout} className="btn btn-success">Checkout</button></td>
						  </tfoot> 
						</table>

						<button onClick={handleClearCart} className="btn btn-warning">Delete Cart</button>
					</React.Fragment> : 'No Orders To Show'
				}
				</div> 
			</div> 
		</div> 
)
}

export default CartForm;