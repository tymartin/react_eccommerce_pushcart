import React, {useState} from "react"


const AddCategoryForm = () => {

	const [formData, setFormData] = useState("")

	const handleCategoryNameChange = (e) => {
		setFormData(e.target.value)
	}

	const handleAddCategory = (e) => {
		e.preventDefault()

		let url = "http://localhost:3001/categories/";
		let data = {name: formData}
		let token =localStorage.getItem('token'); 
		fetch(url, {
			method :"POST",
			body: JSON.stringify(data), 
			headers: {
				"Content-Type" : "application/json", 
				"Authorization" : token
			}
		})
		.then( data=>data.json())
		.then( category=>console.log(category))
	}

	return(

		<div> 
			<h2><strong> Add New Category Here </strong> </h2> 
			<form action="" onSubmit={handleAddCategory}> 
				<div className="form-group text-warning"> 
					<label htmlfor="name"> Category Name:</label> 

					<input
						type="text"
						className="form-control mb-1"
						id="name"
						name="name"
						onChange={handleCategoryNameChange}
						/>
						</div> 
							
			
						<button className="btn btn-success mb-4"> Add New Category</button>

			</form> 
		</div>
	 )
}


export default AddCategoryForm; 