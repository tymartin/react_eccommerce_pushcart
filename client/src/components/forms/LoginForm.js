import React, {useState} from "react"


const LoginForm = ({handleToken, handleUserLogin}) => {

	const [formData, setFormData] = useState({
		email: null,
		password: null 
	})

	const handleChange = e => {
		setFormData({
			...formData, 
			[e.target.name] : e.target.value
		})
	}

	const [result, setResult] = useState({
		message: null,
		successful: null 
	})

	const handleLogin = e => {
		e.preventDefault(); 

		let data = {
			email: formData.email,
			password: formData.password,
		}
		fetch('http://localhost:3001/users/login', {
			method: "POST",
			body: JSON.stringify(data), 
			headers : {
				"Content-Type" : "application/json"
			}
		})
		.then( data => data.json())
		.then( user => {
			console.log(user)

			if(user.token) {
			setResult({
				successful: true,
				message: user.message
			})
			localStorage.setItem('user', JSON.stringify(user.user));
			localStorage.setItem('token', "Bearer " + user.token);  

			} else {
				setResult({
				 successful: false,
				 message: user.message
				})
			}
		})
	}

	const resultMessage = () => {

		let classlist;
		if(result.successful ===true) {
			classlist = 'alert alert-success'
		} else {
			classlist = 'alert alert-danger'
		}
		return (
			<div className={classlist}> 
			{result.message}
			</div>
			)
	}

	return(


		<div className="container col-6"> 

			<form action="" onSubmit={handleLogin}> 
				<div className="card mt-3  text-white">
				{result.successful == null ? "" : resultMessage()}
					<h3 className="card-header bg-warning text-secondary"><strong>Login Form</strong></h3> 
					<div className="card-body bg-dark px-3"> 
					
						<input
						type="email"
						className="form-control mx-auto mb-3"
						placeholder="Username"
						id="email"
						name="email"
						onChange={handleChange}
						/>

						<input
						type="password"
						className="form-control mx-auto mb-3"
						placeholder="Password"
						id="password"
						name="password"
						onChange={handleChange}
						/>

						<button className="btn btn-success mb-1">Log In</button> 
					</div> 			
				</div> 
			</form> 
		</div> 

		)

}

export default LoginForm; 