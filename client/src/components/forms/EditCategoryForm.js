import React, {useState} from "react"


const EditCategoryForm = ({categories, handleChangeCategoriesStatus, categoriesStatus}) => {

const [selectedCategory, setSelectedCategory] = useState({
	name: null, 
	id: null, 

})

	const handleChangeSelected = (e) => {
		let categorySelected = categories.find(category => { 
		if(category._id == e.target.value) {
			return category.name
			}
		})
		setSelectedCategory({
			id: e.target.value,
			name: categorySelected.name
		})
	}

	const handleChangeName = e => {
		setSelectedCategory ({
			...selectedCategory,
			[e.target.name] : e.target.value
		})
	}

	const handleEditCategory = e => {
		e.preventDefault(); 

		fetch("http://localhost:3001/categories/" + selectedCategory.id, {
			method: "PUT",
			body : JSON.stringify({ name:selectedCategory.name}), 
			headers: {
				"content-Type": "application/json",
				"Authorization" : localStorage.getItem('token')
			}
		})
		.then(data => {
			return data.json()})
		.then(result => 
			handleChangeCategoriesStatus({
				lastUpdated : selectedCategory.id,
				status: null, 
				isLoading: true
			})
		)
	}

	return(

		<div> 
		<hr/>
		<hr/>
			<h2><strong> Update Category Here </strong> </h2> 
			<form action="" onSubmit={handleEditCategory}>
			 { 
			 	categoriesStatus.isLoading ? 
			 	<div class="spinner-border text-primary" role="status">
				   <span class="sr-only">Loading...</span>
				</div>
				:
				<React.Fragment> 
				<div className="form-group text-warning"> 
					

					<div className="form-group"> 
						<label htmlfor="category"> Category Name:</label> 
						<select 
						type="text"
						className="form-control"
						id="category"
						name="category"
						onChange={handleChangeSelected}
						>
						<option disabled selected> Select Category </option> 
						{categories.map(category=> {
							return (
							<option value={category._id}>{category.name}</option>
								)
						})
						}
						</select>
					</div>  
						<label htmlfor="name"> New Category Name:</label> 
					<input
						type="text"
						className="form-control"
						id="name"
						name="name"
						value={selectedCategory.name}
						onChange={handleChangeName}
						/>
						</div> 
							
					
						<button className="btn btn-success mb-4"> Update Category</button>
				
				</React.Fragment> 
			 } 

			</form> 
		</div>
	 )
}


export default EditCategoryForm; 