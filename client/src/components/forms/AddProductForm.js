import React, {useState} from "react"


const AddProductForm = ({categories}) => {

const [product, setProduct] = useState({
	name: null,
	description: null,
	price: null,
	categoryId: null,
	image: null
});

const onChangeText = e => {
	setProduct({
		...product, 
		[e.target.name] : e.target.value
	})
}

const handleChangeFile = e => {
	console.log(e.target.files[0])
	setProduct({
		...product,
		image: e.target.files[0]
	})
}

const handleAddProduct = e => {
	e.preventDefault();
	const formData = new FormData(); 

	formData.append('name',product.name)
	formData.append('price',product.price)
	formData.append('categoryId',product.categoryId)
	formData.append('description',product.description)
	formData.append('image',product.image)

	let url = "http://localhost:3001/products/";
		let data = {name: formData}
		let token =localStorage.getItem('token'); 
		fetch(url, {
			method :"POST",
			body: formData, 
			headers: {
				"Authorization" : token
			}
		})
		.then( data=>data.json())
		.then( product=>console.log(product))

}
	return(
		<div class="container">
		
		
				<h2><strong> Add Product Here </strong> </h2> 
				<form action=""  enctype="multipart/form-data" onSubmit={handleAddProduct}> 
					<div className="form-group"> 
						<label htmlfor="name"> Product Name:</label> 
						<input
						type="text"
						className="form-control"
						id="name"
						name="name"
						onChange={onChangeText}
						/>
					</div> 
					
					<div className="form-group"> 
						<label htmlfor="price"> Product Price:</label> 
						<input
						type="number"
						className="form-control"
						id="price"
						name="price"
						onChange={onChangeText}
						/>
					</div>

						<div className="form-group"> 
						<label htmlfor="category"> Category Name:</label> 
						<select 
						type="string"
						className="form-control"
						id="categoryId"
						name="categoryId"
						onChange={onChangeText}
						>
						<option disabled selected> Select Category </option> 
						{categories.map(category=> {
							return (
							<option value={category._id}>{category.name}</option>
								)
						})
						}
						</select>
					</div>  

					<div className="form-group"> 
						<label htmlfor="description"> Description:</label> 
						<textarea
						type="string"
						className="form-control"
						id="description"
						name="description"
						onChange={onChangeText}
						>
						</textarea>
					</div> 
			
					<div className="form-upload"> 
						<input
						type="file"
						name="image"
						id="image"
						className="file"
						onChange={handleChangeFile}
						> 
						</input>			
					</div>
					
					<hr/> 
					<button className="btn btn-success mb-4"> Add New Product</button>


				</form>
				
			

	

		 
			
		</div>

		)

}


export default AddProductForm; 