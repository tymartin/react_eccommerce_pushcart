import React, {useState} from "react"


const DeleteProductForm = ({products}) => {

	const [selectedProduct, setSelectedProduct] = useState({
		_id: null,
		name: null 
	})

	const handleChange = e => {
		setSelectedProduct({
			_id: e.target.value 
		})
	}

	const handleDeleteProduct = e => {
		e.preventDefault();

		let url = 'http://localhost:3001/products/' + selectedProduct._id; 

		fetch(url, {
			method: "DELETE",
			headers: {
				"Authorization": localStorage.getItem('token')
			}
		})
		.then(data => data.json())
		.then(result => console.log(result))
	}

	return(

		<div> 
		<hr/>
		<hr/>
			<h2><strong> Delete Category Here </strong> </h2> 
			<form onSubmit={handleDeleteProduct} action=""> 
				<div className="form-group text-primary"> 
				
					<div className="form-group"> 
						<form action="" onSubmit={handleDeleteProduct}> 
						<label htmlfor="productId"> Select Product Name:</label> 
						<select 
						name="productId"
						className="form-control"
						id="product"
						name="product"
						onChange={handleChange}
						>
					{
					products.map(product=>{
					return (
					<option value={product._id}> {product.name} </option> 
					)
					})}  
						</select>
						</form> 
					</div> 
					
					<button className="btn btn-danger mb-4"> Delete Product</button> 
					</div> 
			</form> 
		</div>
	 )
}


export default DeleteProductForm; 
