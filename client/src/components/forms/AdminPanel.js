import React from "react"
import {BrowserRouter as Router, Switch, Route} from "react-router-dom"


import AddProductForm from './AddProductForm'; 
import EditProductForm from './EditProductForm';
import DeleteProductForm from './DeleteProductForm';  
import AddCategoryForm from './AddCategoryForm';
import EditCategoryForm from './EditCategoryForm'; 
import DeleteCategoryForm from './DeleteCategoryForm'; 

const AdminPanel = ({categories, handleChangeCategoriesStatus, categoriesStatus, products, handleChangeProductsStatus, productsStatus }) => {

const backgroundColor = {
  backgroundColor:"#EBC715"
};

const backgroundColor2 = {
  backgroundColor:"#01117F"
};
	return (

		<div className="container">
			<div className="row">
				<div className=" col-lg-6 pt-3 mt-5 text-primary" style={backgroundColor}> 
			
         	   <AddProductForm categories={categories}/> 
         	   			
         		<EditProductForm categories={categories}
         		products={products}
         		handleChangeProductsStatus={handleChangeProductsStatus}
         		productsStatus={productsStatus}
         		/> 

         		<DeleteProductForm products={products}/>
         	
         	 	</div>
         	 	<div className="col-lg-6 pt-3 mt-5 text-warning" style={backgroundColor2} > 
         	 	<AddCategoryForm/> 

         	 	<EditCategoryForm 
         	 		categories={categories}
         	 		handleChangeCategoriesStatus={handleChangeCategoriesStatus}
         	 		categoriesStatus={categoriesStatus}
         	 		/> 
         	 	<DeleteCategoryForm categories={categories}/> 
         	 	</div>
         	</div> 
        </div> 
		)
}


export default AdminPanel; 