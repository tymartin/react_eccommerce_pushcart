import React, {useState} from "react"


const DeleteCategoryForm = ({categories}) => {

	const [selectedCategory, setSelectedCategory] = useState({

})

	const handleChangeSelected = (e) => {
		let categorySelected = categories.find(category => { 
			if (category._id == e.target.value)
			{
				return category.name
			}
		})
		setSelectedCategory({
			id: e.target.value,
			name: categorySelected.name
		})
	}

	const handleDeleteCategory = e => {
		e.preventDefault(); 

		fetch("http://localhost:3001/categories/" + selectedCategory.id, {
			method: "DELETE",
			body : JSON.stringify({ name:selectedCategory.name}), 
			headers: {
				"content-Type": "application/json",
				"Authorization" : localStorage.getItem('token')
			}
		})
		.then(data => data.json())
		.then(result => console.log(result))
	}

	return(

		<div> 
		<hr/>
		<hr/>
			<h2><strong> Delete Category Here </strong> </h2> 
			<form action="" onSubmit={handleDeleteCategory}> 
				<div className="form-group text-warning"> 
				
					<div className="form-group"> 
						<label htmlfor="category"> Select Category Name:</label> 
							<select 
						type="string"
						className="form-control"
						id="category"
						name="category"
						onChange={handleChangeSelected}
						>
						<option disabled selected> Select Category </option> 
						{categories.map(category=> {
							return (
							<option value={category._id}>{category.name}</option>
								)
						})
						}
						</select>
					</div> 
					
					<button className="btn btn-danger mb-4"> Delete Category</button> 
					</div> 
			</form> 
		</div>
	 )
}


export default DeleteCategoryForm; 