import React, {useState, useEffect} from 'react'; 
import TransactionDetails from './TransactionDetails'
import TransactionHead from './TransactionHead'



const TransactionForm = ({transaction}) => {

const [transactions, setTransaction] = useState ([])

useEffect(() => {
	fetch('http://localhost:3001/transactions', {
		headers: {
			"Authorization": localStorage.getItem('token')
		}
	})
	.then(data => data.json())
	.then(transactions => setTransaction(transactions))
}, [])

	return (

			<React.Fragment> 
				<h1 className="bg-dark text-white col-10 mt-5 ml-5">Transactions</h1> 
					<div class="accordion col-10 ml-5" id="accordionExample">
					          {
					      	transactions.map( transaction => {
					      		return (
					
					  <div class="card">
					    <div class="card width-50" id="headingOne">
					      <h6 class="mb-0">
					        <button class="btn btn-link" 
					        type="button" 
					        data-toggle="collapse" 
					        data-target={"#collapseOne"+transaction._id} 
					        aria-expanded="true" 
					        aria-controls="collapseOne">

					         	<TransactionHead
					         	transaction={transaction}/> 
					         	
					        </button>
					      </h6>
					    </div>

					  <div id={"collapseOne"+transaction._id} class="collapse" aria-labelledby="headingOne" data-parent="#accordionExample">
					      <div class="card-body width-50"> 
					    
					      			<TransactionDetails
					      		    transaction={transaction}/> 
					      	
	

					      </div>
					    </div>
					  </div>
					)})}
					</div>
			</React.Fragment> 
		)
}


export default TransactionForm;