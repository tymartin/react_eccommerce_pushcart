import React, {useState} from "react"


const EditProductForm = ({categories, products, handleChangeProductsStatus, productsStatus}) => {

	const [selectedProduct, setSelectedProduct] = useState({
	name: null, 
	_id: null, 
	price: null, 
	categoryId: null, 
	description: null, 
	categoryName: null 

})
const handleChangeSelected = (e) => {
		let productSelected = products.find(product => { 
		return (product._id == e.target.value) 
		})
		let currCategory = categories.find(category => {
			return category._id === productSelected.categoryId
		})
		setSelectedProduct({
			...productSelected,
			image: "http//localhost:3001" + productSelected.image, 
			categoryName: currCategory.name
		})
	}

const handleChangeName = e => {
		setSelectedProduct ({
			...selectedProduct,
			[e.target.name] : e.target.value
		})
		console.log(typeof selectedProduct.image)
	}

const handleChangeFile = e => {
	console.log(e.target.files[0])
	setSelectedProduct({
		...selectedProduct,
		image: e.target.files[0]
	})

	console.log(selectedProduct)
}

const handleEditProduct = (e) => {
e.preventDefault();
// console.log(selectedProduct);

let formData = new FormData();

formData.append('name', selectedProduct.name);
formData.append('price', selectedProduct.price);
formData.append('categoryId', selectedProduct.categoryId);
formData.append('description', selectedProduct.description);

if(typeof selectedProduct.image === 'object') {
formData.append('image', selectedProduct.image);

}
console.log(formData.get('image'));

let url = "http://localhost:3001/products/"+selectedProduct._id;

fetch(url, {
method : "PUT",
headers : {
"Authorization" : localStorage.getItem('token')
},
body: formData
})
.then( data => data.json())
.then( result => console.log(result))
}


	return(
		<div class="container">
		
		
				<h2><strong> Update Product Here </strong> </h2> 
				<form action="" onSubmit={handleEditProduct} > 

					<div className="form-group"> 
						<label htmlfor="products"> Product Lists:</label> 
							<select 
						type="text"
						className="form-control mb-1"
						id="product"
						name="product"
						onChange={handleChangeSelected}
						>
						{products.map(product=> {
							return (
							<option value={product._id}>{product.name}</option>
								)
						})
						}
						</select>
					</div>  

					
						<label htmlfor="image"> Product Image:</label> 
						<hr/>
						<img src={selectedProduct.image} height="240"/> 
						<hr/>
				
					<div className="form-group"> 
						<label htmlfor="name"> Product Name:</label> 
						<input
						type="text"
						className="form-control"
						id="name"
						name="name"
						value={selectedProduct.name}
						onChange={handleChangeName}
						/>
					</div> 
					
					<div className="form-group"> 
						<label htmlfor="price"> Product Price:</label> 
						<input
						type="number"
						className="form-control"
						id="price"
						name="price"
						value={selectedProduct.price}
						onChange={handleChangeName}
						/>
					</div>

					<div className="form-group"> 
					<label htmlfor="categoryId"> Category:</label> 
					<select
						type="text"
						className="form-control mb-1"
						id="categoryId"
						name="categoryId"
						value={selectedProduct.categoryId}
						onChange={handleChangeName}
						>
				 	 {
					categories.map(category=>{
					return (
					category._id === selectedProduct.categoryId ?
					<option value={category._id}> {category.name} </option> :
					<option value={category._id} selected> {category.name} </option>
					)
					})}  
						</select> 	
					</div>  

					<div className="form-group"> 
						<label htmlfor="description"> Description:</label> 
						<textarea
						type="string"
						className="form-control"
						id="description"
						name="description"
						value={selectedProduct.description}
						onChange={handleChangeName}
						>
						</textarea>
					</div> 
			
					<div className="form-upload"> 
						<input
						type="file"
						name="image"
						id="image"
						className="file"
						onChange={handleChangeFile}
						> 
						</input>			
					</div>
					
					<hr/> 
					<button className="btn btn-success mb-4"> Update Product</button>


				</form>
				
			

	

		 
			
		</div>

		)

}


export default EditProductForm; 