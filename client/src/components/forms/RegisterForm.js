	import React, {useState} from "react"


const RegisterForm = () => {

	const [ formData, setFormData] = useState({
		firstname: "",
		lastname: "",
		email: "",
		password: "",
		confirmPassword: ""
	}); 

	const { firstname, lastname, email, password, confirmPassword} = formData

	const onChangeHandler = e => {
		setFormData({
			...formData, 
			[e.target.name]: e.target.value
		})
	}

	const handleRegister = e => {
		e.preventDefault();
		if(password !== confirmPassword){
			alert("Passwords do not match")
		} else {
			fetch("http://localhost:3001/users/register", {
				method: "POST",
				headers: {
					"Content-Type": "application/json"
				}, 
				body: JSON.stringify(formData)
			})
			.then(data => data.json())
			.then(user => {
				console.log(user.message)
				setFormData({
					firstname: "",
					lastname: "",
					email: "",
					password: "",
					confirmPassword: ""
				})
				if (user.message) {
					let element = document.getElementById("message")
					element.innerHTML = user.message
					element.classList.toggle('d-none')
					setTimeout(function() {
					element.classList.toggle('d-none')
					}, 3000) 
				}
					if (user.sucessMessage) {
					let element = document.getElementById("smessage")
					element.innerHTML = user.sucessMessage
					element.classList.toggle('d-none')
					setTimeout(function() {
					element.classList.toggle('d-none')
					}, 9000) 
				}
			})
		}
	}

	

	return(
		<div className="container">
			<div className="bg-warning col-lg-6 mx-auto pt-3 mt-5 text-primary"> 
			<div className="alert alert-danger d-none" role="alert" id="message"/>  
			<div className="alert alert-success d-none" role="alert" id="smessage">
			You are Now Registered!
			</div> 
			<h2><strong> Online Registration Form </strong></h2> 
				<form onSubmit={ e=> handleRegister(e)}> 
					<div className="form-group"> 
						<label htmlfor="firstname"> First Name:</label> 
						<input
						type="text"
						className="form-control"
						id="firstname"
						name="firstname"
						value={firstname}
						onChange={ e => onChangeHandler(e)}
						/>
					</div> 

					<div className="form-group"> 
						<label htmlfor="lastname"> Last Name:</label> 
						<input
						type="text"
						className="form-control"
						id="lastname"
						name="lastname"
						value={lastname}
						onChange={ e => onChangeHandler(e)}
						/>
					</div> 

					<div className="form-group"> 
						<label htmlfor="email"> Email:</label> 
						<input
						type="text"
						className="form-control"
						id="email"
						name="email"
						value={email}
						onChange={ e => onChangeHandler(e)}
						/>
					</div> 

					<div className="form-group"> 
						<label htmlfor="Password"> Password:</label> 
						<input
						type="password"
						className="form-control"
						id="password"
						name="password"
						value={password}
						onChange={ e => onChangeHandler(e)}
						/>
					</div>

					<div className="form-group"> 
						<label htmlfor="confirmPassword"> Confirm Password:</label> 
						<input
						type="password"
						className="form-control"
						id="confirmPassword"
						name="confirmPassword"
						value={confirmPassword}
						onChange={ e => onChangeHandler(e)}
						/>
					</div>

					<button type="submit" className="btn btn-success mb-4"> Register </button> 
				</form> 
			</div> 
		</div>

		)

}


export default RegisterForm; 
