import React from 'react';

const Catalog = ({products, handleAddToCart}) => {
	
	
	return (

		<div className="container">
			<h1>Products</h1>
			<div className="row">
				
					{
						products.map( product => {
							return (
								<div className="col-12 col-md-3">
								<div className="card bg-secondary text-light">
								<img src={"http://localhost:3001" + product.image} height="98"/>
									<div className="card-header text-warning"><strong><center>{product.name}</center></strong>
									</div>
									<div className="card-body">
										<p className="card-item">Price: {product.price}</p>
										<p className="card-item">{product.description}</p>
										<button onClick={()=>{handleAddToCart(product)}} className="btn btn-warning" type="submit">Add to Cart</button>
									</div>

								</div>
								</div>
							)
						})
					}
				
			</div>
		</div>
	)
}

export default Catalog;




