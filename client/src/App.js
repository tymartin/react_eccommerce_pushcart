import React, { useEffect, useState } from 'react';
import {BrowserRouter as Router, Switch, Route} from "react-router-dom"
import Navbar from './components/layouts/Navbar'; 
import Catalog from './components/Catalog'; 

//FORMS 
import AdminPanel from './components/forms/AdminPanel'; 
import RegisterForm from './components/forms/RegisterForm';
import LoginForm from './components/forms/LoginForm';
import CartForm from './components/forms/CartForm'; 
import TransactionForm from './components/forms/TransactionForm'; 


function App() {

//states
  const [products, setProducts] = useState([])
  const [cart, setCart] = useState([])
  const [transaction, setTransaction] = useState([])
  const [total, setTotal] = useState([0])
  const handleAddToCart = (product) => {
    let matched = cart.find(item => {
      return item._id === product._id; 
    })
    console.log(matched)
   if(!matched){
    setCart([
      ...cart,
      product
      ])
  }
  }

  const handleClearCart = () => {
    setCart([])
  }

  const handleRemoveItem = (itemTobeRemove)=> {
    let updatedCart = cart.filter( item=>item !== itemTobeRemove)
    setCart(updatedCart)
  }
  const [productsStatus, setProductsStatus] = useState({
    lastUpdated: null, 
    status: null
  })
  const [categories, setCategories] = useState([])
  const [categoriesStatus, setCategoriesStatus] = useState({
    lastUpdated: null, 
    status: null, 
    isLoading: false
  })
  const [user, setUser] = useState ({
    firstname: null, lastname: null, role: null, id: null 
  }
  ) 
const [token, setToken] = useState(""); 

useEffect(()=> {
  let total = 0;
  cart.forEach(item=>{
    total += item.price
  })

  console.log(total)
  setTotal(total)
},[cart])

useEffect(()=>{
    fetch("http://localhost:3001/categories", {
      method: "GET"
    })
     .then(res=>res.json())
      .then(data => {
        setCategories([...data])
        setCategoriesStatus({isLoading : false})
        })
},[categoriesStatus.isLoading])

 useEffect(() => {
     fetch("http://localhost:3001/products")
     .then(res=>res.json())
     .then(data => {
      setProducts([...data])
     })

     fetch("http://localhost:3001/categories")
     .then(res=>res.json())
      .then(data => {
        setCategories([...data])
        })
      },[]); 
 
 //change state methods 
 const handleChangeCategoriesStatus = (status) => {
  setCategoriesStatus(status)
 }

 const handleChangeProductsStatus = (status) => {
  setProductsStatus(status)
 }

  return (
    <Router>
    <div>
     <Navbar/> 
        <Switch> 
          <Route exact path="/">
            <Catalog products={products}
            handleAddToCart={handleAddToCart}/> 
          </Route>

          <Route exact path="/admin-panel">
            <AdminPanel 
              categories={categories}
              handleChangeCategoriesStatus={handleChangeCategoriesStatus}
              categoriesStatus={categoriesStatus}
              products={products}
              handleChangeProductsStatus={handleChangeProductsStatus}
              productsStatus={productsStatus}
            /> 
          </Route>

            <Route exact path="/register">
            <RegisterForm/> 
          </Route>

             <Route exact path="/login">
            <LoginForm/> 
          </Route>

           <Route exact path="/cart">
            <CartForm cart={cart}
            handleClearCart={handleClearCart}
            handleRemoveItem={handleRemoveItem}
            total={total}/> 
          </Route>

           <Route exact path="/transaction">
            <TransactionForm
            cart={cart}
            transactiob={transaction}/> 
          </Route>

        </Switch> 
    </div>
    </Router>
  )
}

export default App;
